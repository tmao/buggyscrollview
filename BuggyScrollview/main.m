//
//  main.m
//  BuggyScrollview
//
//  Created by Toby Mao on 3/11/14.
//  Copyright (c) 2014 Next Issue Media. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BuggyScrollviewAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BuggyScrollviewAppDelegate class]));
    }
}
