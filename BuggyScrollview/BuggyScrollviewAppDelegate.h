//
//  BuggyScrollviewAppDelegate.h
//  BuggyScrollview
//
//  Created by Toby Mao on 3/11/14.
//  Copyright (c) 2014 Next Issue Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuggyScrollviewAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
