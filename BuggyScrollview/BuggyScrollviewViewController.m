//
//  BuggyScrollviewViewController.m
//  BuggyScrollview
//
//  Created by Toby Mao on 3/11/14.
//  Copyright (c) 2014 Next Issue Media. All rights reserved.
//

#import "BuggyScrollviewViewController.h"

@interface BuggyScrollviewViewController ()

@end

@implementation BuggyScrollviewViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Create a scrollView that is bigger than ~1950 on an iPad.
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 2048, 1024)];
    scrollView.contentSize = CGSizeMake(2048, 2048);
    scrollView.backgroundColor = [UIColor yellowColor];
    // Set pagingEnabled to be True. If this is false, there is no problem.
    scrollView.pagingEnabled = YES;
    scrollView.delegate = self;
    
    UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 200, 200)];
    myView.backgroundColor = [UIColor redColor];
    [scrollView addSubview:myView];
    
    [self.view addSubview:scrollView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"%@", NSStringFromCGPoint(scrollView.contentOffset));
}
@end
